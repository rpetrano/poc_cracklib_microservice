Usage:

```shell
docker build -t poc_cracklib_microservice .
docker run -dp 12345:12345 poc_cracklib_microservice

curl -X POST 'http://localhost:12345/check_password' --data 'password=bokbok'
curl -X POST 'http://localhost:12345/check_password_change' --data 'new_password=ZaK421!kj&old_password=ZaK412!kj'
```

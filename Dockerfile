FROM centos:6
MAINTAINER robert.petranovic@oradian.com
LABEL description="Password strength microservice"

# Add EPEL repo
RUN set -x \
    && rpm --import "https://dl.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-$(rpm -q --queryformat '%{VERSION}' centos-release)" \
    && yum -y install "https://dl.fedoraproject.org/pub/epel/epel-release-latest-$(rpm -q --queryformat '%{VERSION}' centos-release).noarch.rpm"

# Install dependencies
RUN set -x \
    && yum -y update \
    && yum -y install git python{,-devel} python-pip cracklib{,-devel} gcc

ARG GIT_REPO="https://bitbucket.org/rpetrano/poc_cracklib_microservice.git"
ARG GIT_BRANCH="master"

RUN git clone -b $GIT_BRANCH $GIT_REPO

WORKDIR poc_cracklib_microservice
RUN pip install -r requirements.txt

ENV FLASK_APP=main.py
ENV PORT=12345
ENV HOST=0.0.0.0

EXPOSE $PORT

HEALTHCHECK --interval=5s --timeout=1s --retries=3 \
   CMD curl -X GET http://$HOST:$PORT/ping

CMD flask run --port=$PORT --host=$HOST

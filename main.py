import os

import cracklib
from flask import Flask, request, make_response

DICT = os.environ.get("DICT", "/usr/share/cracklib/pw_dict")

app = Flask("poc_cracklib_microservice")

# Required difference from old password
cracklib.DIFF_OK = 5

# Worth of digits
cracklib.DIG_CREDIT = 1

# Worth of lowercase
cracklib.LOW_CREDIT = 1

# Worth of symbols
cracklib.OTH_CREDIT = 1

# Minimal credit of password
cracklib.MIN_LENGTH = 9

@app.route("/ping")
def ping():
    return "pong"

@app.route("/check_password", methods=['POST', 'PUT'])
def check_password():
    password = request.form.get('password', None)

    if password is None:
        return make_response("password not provided!", 400)

    try:
        cracklib.FascistCheck(password, DICT)
        return make_response("", 200)
    except ValueError as reason:
        return make_response(str(reason), 406)

@app.route("/check_password_change", methods=['POST', 'PUT'])
def check_password_change():
    new_password = request.form.get('new_password', None)
    old_password = request.form.get('old_password', None)

    if new_password is None or old_password is None:
        return make_response("new_password or old_password not provided!", 400)

    try:
        cracklib.VeryFascistCheck(new_password, old_password, DICT)
        return make_response("", 200)
    except ValueError as reason:
        return make_response(str(reason), 406)

if __name__ == "__main__":
    app.run()
